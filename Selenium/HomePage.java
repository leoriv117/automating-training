import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.excel.lib.util.Xls_Reader;

public class HomePage {
	 
 ChromeDriver driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public  HomePage(ChromeDriver driver) {
          this.driver = driver;
	}
	
	//Locator for username field
	private By fName = By.id("first-name");
	
	//Locator for password field
	private static By lName = By.id("last-name");
	
	//Locator for login button
	private static By jobTitle = By.id("job-title");
	
	private static By button = By.id("radio-button-3");
	
	private static By checkbox = By.id("checkbox-1");
	
	private static By years = By.id("select-menu");
	
	private static By date= By.xpath("//*[@id=\"datepicker\"]");
	
	private static By submit = By.xpath("/html/body/div/form/div/div[8]/a");
	
	private static By message = By.xpath("/html/body/div/h1");
	
	
	//Method to enter username

	
	public void login() {
		
		  Xls_Reader reader = new Xls_Reader("./datafiles/Datafile.xlsx");
		
		String sheetName = "Hello";
		int rowCount = reader.getRowCount(sheetName);

		for(int rowNum=2; rowNum<=rowCount; rowNum++){
			String firstName = reader.getCellData(sheetName, "First name", rowNum);
			String lastName = reader.getCellData(sheetName, "Last name", rowNum);
			String jTitle = reader.getCellData(sheetName, "Job title", rowNum);
			System.out.println(firstName+ " | " + lastName+" | "+jTitle);
			
			driver.findElement(jobTitle).sendKeys(jTitle);
			driver.findElement(fName).sendKeys(firstName);
			driver.findElement(lName).sendKeys(lastName);

			
		}
	}
	public  void educationLevel() {
		driver.findElement(button).click();
	}
	
	public  void sexCheckbox() {
		driver.findElement(checkbox).click();
	}
	
	public  void yearsExp() {
		Select yearsexp= new Select(driver.findElement(years));
		yearsexp.selectByIndex(1);
		
	}
	
	public  void datePicker(String currentdate) {
		driver.findElement(date).sendKeys(currentdate);
	}
	//Method to click on Login button
	public  void clickSumbit() throws InterruptedException  {
	
			Thread.sleep(3000);
	
			driver.findElement(submit).click();
			
		
	}
	
	public void success () {
		String bodyText = driver.findElement(message).getText();
		Assert.assertTrue("Thanks for submitting your form", bodyText.contains(bodyText));
   }
	
}
