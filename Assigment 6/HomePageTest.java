
import java.io.IOException;
import java.time.Duration;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.excel.lib.util.Xls_Reader;

public class HomePageTest {
         private static ChromeDriver driver;
         static String URL="https://formy-project.herokuapp.com/form";
	
		@BeforeClass
		public static void openBrowser(){
			System.setProperty("webdriver.chrome.driver", "D:/Eclipse/Chromedriver/chromedriver.exe");
            driver= new ChromeDriver(); //Initialising the browser driver
            driver.get(URL);
          
    		
		}
		
		@Test
		  public void submitForm () throws InterruptedException, IOException {
		   HomePage homePage= new HomePage(driver);
		   homePage.login();
		   homePage.educationLevel();
		   homePage.sexCheckbox();
		   homePage.yearsExp();
		   homePage.datePicker("12/12/20");
		   homePage.clickSumbit();
		   homePage.success();
		
}

		 
}
