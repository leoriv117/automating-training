
import java.io.IOException;
import java.time.Duration;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.excel.lib.util.Xls_Reader;

public class HomePageTest {
         private static ChromeDriver driver;
         static String URL="https://www.saucedemo.com/";
	
		@BeforeClass
		public static void openBrowser(){
			System.setProperty("webdriver.chrome.driver", "D:/Eclipse/Chromedriver/chromedriver.exe");
            driver= new ChromeDriver(); //Initialising the browser driver
            driver.get(URL);
          
    		
		}
		//Executes all methods and enter the email and pwd ass arguments
		@Test
		  public void shopingProduct () throws InterruptedException, IOException {
		   HomePage homePage= new HomePage(driver);
		   homePage.signIn("standard_user", "secret_sauce");
		   homePage.search();
		   homePage.address();
		   homePage.shipping();
		   homePage.finish();
		   homePage.home();


}

		 
}
