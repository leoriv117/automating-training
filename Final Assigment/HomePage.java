import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.excel.lib.util.Xls_Reader;

public class HomePage {
	 
 ChromeDriver driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public  HomePage(ChromeDriver driver) {
          this.driver = driver;
	}
	
	//Go to the cart locator
	private By cart = By.xpath("//*[@id=\"shopping_cart_container\"]");//Locator to go to cart summary
	private static By checkoutbtn = By.id("checkout"); //Checkout button locator

	//Submit buttons
    private static By additem1= By.id("add-to-cart-sauce-labs-backpack"); //Add item 1 locator
	private static By submitlogin = By.id("login-button");//Locator for login button
	private static By additem2 = By.id("add-to-cart-sauce-labs-bike-light");// Add item 2 locator
	
	//Enter email and password to sign in
	private static By pwd = By.id("password");
	private static By usrname = By.id("user-name");
	
	//Locators for address personal information page
	private static By fname = By.id("first-name");//First name locator
	private static By lname= By.id("last-name"); //Last name locator
	private static By pCode = By.id("postal-code");//Postal code locator
	private static By cnt = By.id("continue"); //Continue to checkout
		
	//Comparing and finish order method for successful test
	private static By order = By.id("finish");//Order page locator
	private static By backto = By.xpath("//*[@id=\"back-to-products\"]");//Back to orders page
	private static By message = By.xpath("//*[@id=\"checkout_complete_container\"]/h2");//Message to be compared 

	
	//Method to search for the items to add to the cart 
	public void search() throws InterruptedException {
		
		driver.findElement(additem1).click();
		driver.findElement(additem2).click();
		Thread.sleep(3000); //Wait 3 seconds then load the cart page 
		driver.findElement(cart).click();
		driver.findElement(checkoutbtn).click();
		
		
	}
	//Method to sing in to the sauce labs page
	public  void signIn( String usuario, String contra) {
		driver.findElement(usrname).sendKeys(usuario);
		driver.findElement(pwd).sendKeys(contra);
		driver.findElement(submitlogin).click();
		
		
	}
	// Method to confirm address
	public  void address() throws InterruptedException {
		//Excel reader code to read the file and then enter the keys to the webpage
				  Xls_Reader reader = new Xls_Reader("./datafiles/Datafile.xlsx");
			
			String sheetName = "Hello";
			int rowCount = reader.getRowCount(sheetName);

			for(int rowNum=2; rowNum<=rowCount; rowNum++){
				String firstName = reader.getCellData(sheetName, "First name", rowNum);
				String lastName = reader.getCellData(sheetName, "Last name", rowNum);
				String postalCode = reader.getCellData(sheetName, "Postal Code", rowNum);
				System.out.println(firstName+ " | " + lastName+" | "+postalCode);
				
				driver.findElement(pCode).sendKeys(postalCode);
				driver.findElement(fname).sendKeys(firstName);
				driver.findElement(lname).sendKeys(lastName);
				Thread.sleep(2000);
				driver.findElement(cnt).click();
				
			}


		
	}
   // Method to confirm order
	public  void shipping () throws InterruptedException  {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,400)", "");
		Thread.sleep(2000);
		driver.findElement(order).click();
		
	
	}
//	Method to select the payment method
	public  void finish() throws IOException, InterruptedException  {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-500)", "");
		//Compares message for successful order completion
		String bodyText = driver.findElement(message).getText();
		Assert.assertTrue("THANK YOU FOR YOUR ORDER", bodyText.contains(bodyText));
		//Takes a screenshot to show the order is completed 
		File screenshotFile= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File(".//images/orderconfirmation.png"));
		Thread.sleep(1000);
		
}
//	Method to return to the main page
	public void home() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-800)", "");
		driver.findElement(backto).click(); //Return to the main page
		
	}


	
}

