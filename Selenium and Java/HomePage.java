import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	 
 ChromeDriver driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public  HomePage(ChromeDriver driver) {
          this.driver = driver;
	}
	
	//Locator for username field
	private By fName = By.id("first-name");
	
	//Locator for password field
	private static By lName = By.id("last-name");
	
	//Locator for login button
	private static By jobTitle = By.id("job-title");
	
	private static By button = By.id("radio-button-3");
	
	private static By checkbox = By.id("checkbox-1");
	
	private static By years = By.id("select-menu");
	
	private static By date= By.xpath("//*[@id=\"datepicker\"]");
	
	private static By submit = By.xpath("/html/body/div/form/div/div[8]/a");
	
	
	//Method to enter username
	public void enterFirstname(String firstname) {
		driver.findElement(fName).sendKeys(firstname);
	}

	//Method to enter password
	public void enterLastName(String lastname) {
		driver.findElement(lName).sendKeys(lastname);
	}
	
	public void enterJob(String job) {
		driver.findElement(jobTitle).sendKeys(job);
	}
	
	public  void educationLevel() {
		driver.findElement(button).click();
	}
	
	public  void sexCheckbox() {
		driver.findElement(checkbox).click();
	}
	
	public  void yearsExp() {
		Select yearsexp= new Select(driver.findElement(years));
		yearsexp.selectByIndex(1);
		
	}
	
	public  void datePicker(String currentdate) {
		driver.findElement(date).sendKeys(currentdate);
	}
	//Method to click on Login button
	public  void clickSumbit() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(submit).click();
		
		
	}
	

	
}
