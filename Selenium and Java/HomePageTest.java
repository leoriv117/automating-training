
import java.time.Duration;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePageTest {
         private static ChromeDriver driver;
         static String URL="https://formy-project.herokuapp.com/form";
	
		@BeforeClass
		public static void openBrowser(){
			System.setProperty("webdriver.chrome.driver", "D:/Eclipse/Chromedriver/chromedriver.exe");
            driver= new ChromeDriver(); //Initialising the browser driver
            driver.get(URL);
           
		}
		@Test
		  public void submitForm () throws InterruptedException {
		   HomePage homePage= new HomePage(driver);
		   homePage.enterFirstname("Leonardo");
		   homePage.enterLastName("Rivas");
		   homePage.enterJob("Mechatronics");
		   homePage.educationLevel();
		   homePage.sexCheckbox();
		   homePage.yearsExp();
		   homePage.datePicker("12/12/20");
		   homePage.clickSumbit();
		   
		
}

		 
}
